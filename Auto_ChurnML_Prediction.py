#!/usr/bin/env python
# coding: utf-8

# Churn Prediction
# ==
# 
# An investigation to see the tends and behaviour that lead to churn.
# 
# The dataset/database that will be considered and applied in the prediction is search-log data:
# 
# 1. search log
# - view log
# - download log
# - user log (a list of user_id and corresponding uid)
# 
# 
# 
# The search log data is passed to KJ's algorithm for preprocessing after Jazz's team's preprocessing sequence. This generates the fasttext dataframes level 2 dataset/data which are used in this analysis. The analysis is particularly focused on few important features, namely:
# 
# 1. `Page_count` in both level 1 and level 2 fasttext data.
# 2. `T_Sum` in the level 1 and `SS_T_Sum` in level 2 fasttext data.
# 3. `T_Mean` in the level 1 and `SS_T_Mean` in level 2 fasttext data.
# 4. `T_Std` in in the level 1 and `SS_T_Std` in level 2 fasttext data.
# 
# The #2 to #4 above is repeated for `view_log` as well. 
# 
# 5. The time difference between each `browsing_id` session, labelled as `browse_diff` and time difference between `T_First` and `T_Last` in both level 1 and the corresponding `SS_T_First` and `SS_T_Last` in level 2 fasttext. 
# 6. Also looks into the VL series which is the data derived from the `view_log`. The `view_diff` is the time different between first and last time images viewed i.e.`SS_VL_T_First` and `SS_VL_T_Last` in level 2 fasttext 
# 
# The features above acts as the main data for the behaviour analysis. This data need to be predict the probability of the users to convert i.e. make their first transaction. With that in mind, this analysis/prediction focuses on new registered user whom have not made any transaction.
# 
# The above five features are not the only features used in the study. 
# 
# All the features from the level 2 data file are as follow :
# 
# `['user_id','uid_y', 'browsing_id','SS_T_First', 'SS_T_Last','SS_DL_T_First','SS_DL_T_Last', 'SS_Page_Count_Sum','SS_Page_Count_Mean', 'SS_Page_Count_Std','SS_T_Sum_Sum', 'SS_T_Sum_Std', 'SS_T_Mean_Sum', 'SS_T_Mean_Std', 'SS_T_Std_Sum', 'SS_T_Std_Std', 'SS_DL_Count_Sum', 'SS_T_Sum_Mean', 'SS_T_Mean_Mean', 'SS_T_Std_Mean', 'SS_VL_Count_Sum', 'SS_VL_Count_Mean', 'SS_VL_Count_Std', 'SS_VL_T_Sum_Sum', 'SS_VL_T_Sum_Mean', 'SS_VL_T_Sum_Std', 'SS_VL_T_Mean_Sum', 'SS_VL_T_Mean_Mean', 'SS_VL_T_Mean_Std', 'SS_VL_T_Std_Sum', 'SS_VL_T_Std_Mean', 'SS_VL_T_Std_Std', 'SS_VL_T_First', 'SS_VL_T_Last', 'view_diff','browse_diff', 'first_last_diff']`
# 
# Most of the features related to sales such as `['packagetype','package','fusd_net','transtype','country', 'purchasetype','user_name','pdate','reg_date','reg_pdate_diff','TR_T_First','TR_T_Last']` can be ignored for prediction churn. However, these features are still required for the churn model training (to label the behaviour) but this is not covered here.

# In[25]:


import pandas as pd
from joblib import load
import numpy as np
import zipfile
import s3fs


# Churn Analysis
# ==
# This is to analysis and predict which users are going to churn based on the search log data. 
# 
# #### What is happening?
# 
# The preprocessed data file (after KJ's algo) is used in this. After the final preprocessing above, the data is loaded into the ML model developed previously and then the prediction is saved into the s3.
# 
# Take note that the `predict_results` function MUST be placed in the same filepath as the `churn_model_.joblib`.

# In[2]:


def load_search(file_path):
    """
    This function reads the csv file from the s3 bucket and converts the dates columns to datetime type  values. 
    Returns a dataframe with date columns converted to datetime format.
    
    Parameters
    ----------
    month : str
        The file path of the preprocessed search+download+view_log. 


    Returns
    -------
    Dataframe object
        The dataframe of the requested month.
        
    """
    month_2018 = pd.read_csv(file_path)
    month_2018.SS_T_First = pd.to_datetime(month_2018.SS_T_First)
    month_2018.SS_T_Last = pd.to_datetime(month_2018.SS_T_Last)
    month_2018.SS_DL_T_First = pd.to_datetime(month_2018.SS_DL_T_First)
    month_2018.SS_DL_T_Last = pd.to_datetime(month_2018.SS_DL_T_Last)
    month_2018.SS_VL_T_First = pd.to_datetime(month_2018.SS_VL_T_First)
    month_2018.SS_VL_T_Last = pd.to_datetime(month_2018.SS_VL_T_Last)

    return(month_2018)


# In[3]:


def search_user(search_data, user_path):
    """
    This function merges the process search_data data(frame) and user data(frame). This is to get the 
    user_id and the corresponding user_name into the same dataframe.
    
    Please take note if the search_data has the `uid` features with valid values. If `uid` feature is NOT PRESENT,
    the resulting merge will give `uid`, not `uid_y`.
    
    Also, the user datafile has to be in the same format as the user datafile found in: 
    s3://123rf-search-log-dumps/output/misc_201911/user.csv
    
    In the conclusion, just make sure that at the end, the dataframe returns has the column `uid_y` after merge.
    
    Parameters
    ----------
    search_data : dataframe object (pandas)
        The dataframe that was preprocessed with KJ's algorithm. 
        
    user_path : str
        The file path for the user csv file. This csv file should have the user_id and the corresponding uid (user_name).
        It is important that the first column in this file is named user_id and the second column named uid. 


    Returns
    -------
    search_data : dataframe object (pandas)
        The dataframe with the processed search log and the added uid as uid_y.

    
    """
    user = pd.read_csv(user_path)

    search_data.user_id = search_data.user_id.astype('int')
    user.user_id = user.user_id.astype('int')
    
    search_data = search_data.merge(user, how='left', left_on='user_id', right_on='user_id')    
    
    return(search_data)


# In[20]:


def get_features(class_data_1):
    """
    This function is mainly used for feature engineering preprocessing step, namely to get the:
    1) 'browse_diff' (difference between browsing time)
    2) 'first_last_diff' (different between first and last browsing session)
    3) 'view_diff' (different between first and last images viewed). 
    
    But before getting these two features, the data has to be sorted using 
    .sort_values(['user_id','SS_T_First','browsing_id']) method, according to the selected features.
    
    This function also selects only the following features for the machine learning prediction
    (and modeling):
    ['user_id','uid_y', \
    'SS_Page_Count_Sum','SS_Page_Count_Mean',\
    'SS_Page_Count_Std','SS_T_Sum_Sum', 'SS_T_Sum_Std', 'SS_T_Mean_Sum',\
    'SS_T_Mean_Std', 'SS_T_Std_Sum', 'SS_T_Std_Std', 'SS_DL_Count_Sum',\
    'SS_T_Sum_Mean', 'SS_T_Mean_Mean', 'SS_T_Std_Mean',\
    'SS_VL_Count_Sum', 'SS_VL_Count_Mean', 'SS_VL_Count_Std',\
    'SS_VL_T_Sum_Sum', 'SS_VL_T_Sum_Mean', 'SS_VL_T_Sum_Std',\
    'SS_VL_T_Mean_Sum', 'SS_VL_T_Mean_Mean', 'SS_VL_T_Mean_Std',\
    'SS_VL_T_Std_Sum', 'SS_VL_T_Std_Mean', 'SS_VL_T_Std_Std',\
    'view_diff','browse_diff', 'first_last_diff']
    
    Be cautios of the 'uid_y'. Depending on the original search_log data give to the function, `uid_y` might just be 'uid'.
    It is advisable to check this feature/parameter. The class_data_1 in this example has the `uid_y` feature.
    
    
    Parameters
    ----------
    class_data_1 : dataframe object (pandas)
        Pandas dataframe object generated by the search_user data merging.

    
    Returns
    -------
    class_data_1 : dataframe object (pandas) 
        Pandas dataframe object with additional new engineered features, namely `view_diff`,`browse_diff` and 
        `first_last_diff`.
    
    
    """
        
    class_data_1 = class_data_1[~(class_data_1.SS_T_Sum_Sum < 0)]
    class_data_1 = class_data_1[~(class_data_1.SS_Page_Count_Sum == 1)].copy()
    class_data_1.reset_index(inplace=True)
    class_data_1.drop(['index'], axis=1, inplace=True)
    
    class_data_1 = class_data_1.sort_values(['user_id','SS_T_First','browsing_id'])
    
    class_data_1['browse_diff'] = class_data_1.SS_T_First.diff()
    class_data_1.browse_diff = class_data_1.browse_diff.dt.total_seconds()
    
    class_data_1['first_last_diff'] = class_data_1.SS_T_Last - class_data_1.SS_T_First
    class_data_1['view_diff'] = class_data_1.SS_VL_T_Last - class_data_1.SS_VL_T_First

    class_data_1.first_last_diff = class_data_1.first_last_diff.dt.total_seconds()
    class_data_1.view_diff = class_data_1.view_diff.dt.total_seconds()

    class_data_1 = class_data_1.loc[:,['user_id','uid_y',                                         'SS_Page_Count_Sum','SS_Page_Count_Mean',                                       'SS_Page_Count_Std','SS_T_Sum_Sum', 'SS_T_Sum_Std', 'SS_T_Mean_Sum',                                       'SS_T_Mean_Std', 'SS_T_Std_Sum', 'SS_T_Std_Std', 'SS_DL_Count_Sum',                                       'SS_T_Sum_Mean', 'SS_T_Mean_Mean', 'SS_T_Std_Mean',                                       'SS_VL_Count_Sum', 'SS_VL_Count_Mean', 'SS_VL_Count_Std',                                       'SS_VL_T_Sum_Sum', 'SS_VL_T_Sum_Mean', 'SS_VL_T_Sum_Std',                                       'SS_VL_T_Mean_Sum', 'SS_VL_T_Mean_Mean', 'SS_VL_T_Mean_Std',                                       'SS_VL_T_Std_Sum', 'SS_VL_T_Std_Mean', 'SS_VL_T_Std_Std',                                       'view_diff','browse_diff', 'first_last_diff']].copy()
    
    class_data_1.reset_index(inplace=True)
    class_data_1.drop(['index'], axis=1, inplace=True)
    
    first_repeat = class_data_1[~(class_data_1.user_id.duplicated(keep='first'))].index.to_list()

    class_data_1.iloc[0,-2] = class_data_1.iloc[1,-2] - class_data_1.iloc[1,-2]
    class_data_1.loc[first_repeat,'browse_diff'] =  class_data_1.iloc[0,-2]
    
    user = class_data_1.loc[:,['user_id','uid_y']].copy()

    return(class_data_1, user)


# In[5]:


def predict_results(clf_path, data):
    """
    This function take the trained classifier model and the X input features to predict the possible 
    outcome (generate the y) i.e. predict which users are mostly to churn in the nearest future.
    
    Take note that the `predict_results` function MUST be placed in the same filepath as the `churn_model_.joblib`.
    
    Parameters
    ----------
    clf_path : ML classifier
        The trained machine learning model function. 
    
    data : dataframe object (pandas) or array values
        The input features.  
    
    
    Return
    ------    
    y_pred_class : predicted classes
        The predicted classes by the machine learning model. Usually 1 or 0.
        
    y_pred_proba : predicted probablities values
        The predicted probability of being class 1.
    
    """
    # class predictions and predicted probabilities
    seed = 18
    clf = load(clf_path)    
    y_pred_class = clf.predict(data.values)
    y_pred_proba = clf.predict_proba(data.values)
    return(y_pred_class, y_pred_proba)


# In[45]:


def output_file(user_data, y_class, y_proba):
    """
    This function focuses on user_id and uid, extracted or taken out from the search_data. This user information
    is combined with the predicted churn class and the predicted probability for churn. In all, this function
    produces the hotlist.csv file which lists users are mostly to churn in the nearest future.
    
    Take note that `the output_file` function MUST be placed in the same filepath as the `predict_results` and
    `churn_model_.joblib`.
    
    Parameters
    ----------
    user_data : dataframe object (pandas)
        user_id and uid, extracted or taken out from the search_data. Make sure that the values are not shuffled i.e.
        the index must not change from the original.
    
    y_pred_class : dataframe object (pandas)
        The predicted classes by the machine learning model., usually 1 or 0.
        
    y_pred_proba : dataframe object (pandas) 
        The predicted probability of being class 1, usually in the range of 1 to 0. 1 means highest probability to churn
         and 0 means no chance to churn i.e. perfect retention.
    
    
    Return
    ------    
    user_data_group : dataframe object (pandas)
        Returns the merged output of all the parameters. Before that, the output is saved as a file.csv in s3, specifically
        in s3a://logo-generator-kj/rish/. This path can be changed to suit your requirements.
    
    """

    user_data = pd.concat([user_data,pd.Series(y_class)], axis=1, ignore_index=True)
    user_data = pd.concat([user_data,pd.DataFrame(y_proba)], axis=1, ignore_index=True)

    user_data.rename(columns={0 : 'user_id', 1 : 'uid', 2 : 'class', 3 : 'proba_0', 4 : 'proba_1'}, inplace=True)
    user_data_group = user_data.groupby(['user_id']).mean()
    user_data_group.to_csv("s3a://logo-generator-kj/rish/churn_hotlist.csv", index=False)
    return(user_data_group)


# In[ ]:





# Testing the functions using Search Log data
# ===
# 
# 

# In[7]:


# If there is a request for getting the path file as input from user, can run this cell
# s3a://logo-generator-kj/rish/jun2019_w1.csv
# s3a://123rf-search-log-dumps/output/misc_201911/user.csv

# file_path = str(input("Enter the file path to the s3 bucket for the search data file: "))
# user_path = str(input("Enter the file path to the s3 bucket for the user data file: "))


# In[8]:


# This is done as testing.
# The files in this location are the weeekly preprocessed level 2 search log files

day_nov = ['04','11','18','25']

df_nov = pd.DataFrame()
for day in day_nov:
    temp = pd.read_csv("s3a://user-hotlist-output/search_pipeline_output/2019/11/"+day+"/fasttext_combined_level_2.csv")
    df_nov = pd.concat([df_nov,temp])

df_nov.SS_T_First = pd.to_datetime(df_nov.SS_T_First)
df_nov.SS_T_Last = pd.to_datetime(df_nov.SS_T_Last)
df_nov.SS_DL_T_First = pd.to_datetime(df_nov.SS_DL_T_First)
df_nov.SS_DL_T_Last = pd.to_datetime(df_nov.SS_DL_T_Last)
df_nov.SS_VL_T_First = pd.to_datetime(df_nov.SS_VL_T_First)
df_nov.SS_VL_T_Last = pd.to_datetime(df_nov.SS_VL_T_Last)


# In[35]:


user_path = "s3a://123rf-search-log-dumps/output/misc_201911/user.csv"
clf_path = "churn_model_2019.joblib"


# In[ ]:


df_search = load_search(file_path)


# In[ ]:


df_search_2 = search_user(df_nov, user_path)


# In[21]:


df_search_3,user_1 = get_features(df_search_2)


# In[37]:


y_class, y_proba = predict_results(clf_path, df_search_3.iloc[ :,2::])


# In[46]:


file = output_file(user_1, y_class, y_proba)


# In[ ]:





# In[ ]:





# In[ ]:




